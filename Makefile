.DEFAULT_GOAL := help

# Variables
# might need to escape the # characters if you aren't using gawk.
version_in_changelog = $(shell awk '!/^## \[Unreleased\]/ && /^## / {gsub(/\[|\]/, ""); print $$2; exit}' CHANGELOG.md)

# Might need to escape the _ in the regex if youa ren't using gawk.
## Generate help output from MAKEFILE_LIST
help:
	@awk '/^[a-zA-Z_0-9%:\\\/-]+:/ { \
	  helpMessage = match(lastLine, /^## (.*)/); \
	  if (helpMessage) { \
	    helpCommand = $$1; \
	    helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
      gsub("\\\\", "", helpCommand); \
      gsub(":+$$", "", helpCommand); \
	    printf "  \x1b[32;01m%-35s\x1b[0m %s\n", helpCommand, helpMessage; \
	  } \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST) | sort -u
	@printf "\n"

## Grabs current version from CHANGELOG.md, creates a git tag and pushes the tag.
git/release-tag: 
ifeq (,$(wildcard ./CHANGELOG.md))
	$(error CHANGELOG.md does not exist, please create it using https://keepachangelog.com as a template)
else
	$(info pushing version $(version_in_changelog))
	@git pull --quiet --tags
	@git rev-parse $(version_in_changelog) > /dev/null 2>&1 || git tag --annotate $(version_in_changelog) --message 'Release $(version_in_changelog) using Makefile'
	@git push origin --tags
endif

