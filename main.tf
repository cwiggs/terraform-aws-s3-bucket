terraform {
  required_version = "~> 1.1.5"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.74.0"
    }
  }
}

data "aws_caller_identity" "current" {}

locals {
  aws_account_id = data.aws_caller_identity.current.account_id
  tags = {
    testing     = "true"
    environment = "develop"
  }
}

resource "aws_s3_bucket" "test_bucket_logs" {
  bucket = "${var.bucket_name}-logs"
  acl    = "log-delivery-write"
  tags   = merge(local.tags, { Name = "${var.bucket_name}-logs" })
}

resource "aws_kms_key" "s3_bucket" {
  description         = "Used to encrypt the test s3 bucket"
  enable_key_rotation = true
  tags                = local.tags
}

resource "aws_s3_bucket" "test" {
  bucket = var.bucket_name
  tags   = merge(local.tags, { Name = var.bucket_name })

  logging {
    target_bucket = aws_s3_bucket.test_bucket_logs.id
    target_prefix = "log/"
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.s3_bucket.arn
        sse_algorithm     = "aws:kms"
      }
    }
  }

  versioning {
    enabled = var.versioning_enabled
  }
}
