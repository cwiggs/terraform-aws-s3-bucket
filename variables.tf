variable "bucket_name" {
  description = "(Required) The name of the main bucket."
  type        = string
}

variable "versioning_enabled" {
  description = "(Optional) Do you want to enable s3 object versioning or not?"
  type        = bool
  default     = true
}
