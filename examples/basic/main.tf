provider "aws" {
  region  = var.region
  profile = "nlok_sandbox"
}

module "s3_bucket" {
  source      = "../.."
  bucket_name = var.bucket_name
}
