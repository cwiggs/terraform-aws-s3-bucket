package test

import (
    "fmt"
    "strings"
    "testing"

	"github.com/gruntwork-io/terratest/modules/aws"
	"github.com/gruntwork-io/terratest/modules/random"
	"github.com/gruntwork-io/terratest/modules/terraform"
    // This library is helpful when you want to add more assert statements.
	//"github.com/stretchr/testify/assert"
)

func TestTerraformAwsS3(t *testing.T) {
    t.Parallel()

    // Setup variables used throughout script.
    expectedName := fmt.Sprintf("test-s3-%s", strings.ToLower(random.UniqueId()))
    awsRegion :=    "us-east-1"

    // Set basic terraform options.
    terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
        TerraformDir: "../",

        // I'd prefer to move this into a terraform.tfvars file.  The tfvars file will help the example be more complete.
        Vars: map[string]interface{}{
            "bucket_name": expectedName,
            "region":      awsRegion,
        },
    })

    defer terraform.Destroy(t, terraformOptions)
    
    terraform.InitAndApply(t, terraformOptions)

    // Verify the s3 bucket was created in the correct region and with the correct name.
    aws.AssertS3BucketExists(t, awsRegion, expectedName)

    // Verify the s3 logging bucket is created.

    // Verify KMS encryption is enabled.

    // Verify main s3 bucket has logging enabled. 

}
