output "bucket_name" {
  description = "The s3 bucket name."
  value       = aws_s3_bucket.test.id
}
