# terraform-aws-s3-bucket
This terraform module will create an s3 bucket with some opinionated defaults to improve security/observability.

There is an examples directory that you can use to see how to use the module.  Each example has a test
directory with a golang script that you can use to test the module. The tests use the Terratest library.

In order to release a new version of this module you can use the makefile to add a git tag.
The command `make git/release-tag` will grab a the most recent version from the CHANGELOG.md
and push it via git.

## TODO
* Disable http on s3 bucket.
* Add tags var in case users want to pass in their own tags.
* Add object lifecycle rules.
* Add ability to enable replication configs.
* Allow user to provide their own KMS key?

## Authors
- Chris Wiggins (me@cwiggs.com)
